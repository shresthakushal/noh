package com.example.kusha.noh.pillReminder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.kusha.noh.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Wongel on 7/14/17.
 */

public class PillReminderAdapter extends RecyclerView.Adapter<PillVH> {
    private Context context;
    private List<Pill> phoneList = new ArrayList<>();

    public PillReminderAdapter(Context context, List<Pill> phoneList) {
        this.context = context;
        this.phoneList = phoneList;
    }

    @Override
    public PillVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_pill, parent, false);
        return new PillVH(view);
    }

    @Override
    public void onBindViewHolder(final PillVH holder, final int position) {
        Pill obj = phoneList.get(position);
        holder.txtName.setText(obj.getName());
        holder.txtTime.setText(obj.getTime());
    }


    @Override
    public int getItemCount() {
        return phoneList.size();
    }
}
