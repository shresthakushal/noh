package com.example.kusha.noh.pillReminder;

import android.app.ProgressDialog;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.example.kusha.noh.R;
import com.example.kusha.noh.appointment.AddAppointmentActivity;
import com.example.kusha.noh.appointment.AppointmentAdapter;
import com.example.kusha.noh.utils.Constant;
import com.example.kusha.noh.utils.GlobalUtils;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class PillReminderActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager manager;
    private FirebaseFirestore db;
    private FloatingActionButton fabButton;
    private PillReminderAdapter adapter;
    private ProgressDialog progressDialog;
    private ArrayList<Pill> pillArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pillreminder);
        initToolbar();
        findViews();
        evenetHandling();
        initRecycleView();
    }

    private void findViews() {
        db = FirebaseFirestore.getInstance();
        recyclerView = findViewById(R.id.recyclerView);
        fabButton = findViewById(R.id.fabButton);

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (GlobalUtils.getFromPrefBoolean(Constant.Pill_LOAD_DATA, this)) {
            getPillData();
            GlobalUtils.savePrefBoolean(Constant.Pill_LOAD_DATA, false, this);
        }
    }

    private void evenetHandling() {
        fabButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GlobalUtils.navigateActivity(PillReminderActivity.this, false, AddPillActivity.class);
            }
        });
    }

    public void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Pill Reminder");
        getSupportActionBar().setDisplayShowTitleEnabled(true);
    }

    private void showLoading() {
        progressDialog = new ProgressDialog(PillReminderActivity.this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
    }

    public void initRecycleView() {
        manager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(manager);
        adapter = new PillReminderAdapter(this, pillArrayList);
        recyclerView.setAdapter(adapter);
        showLoading();
        getPillData();
    }

    private void getPillData() {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        RealmResults<Pill> realmList = realm.where(Pill.class).findAll();
        List<Pill> sales = new ArrayList<>();
        sales = realm.copyFromRealm(realmList);
        realm.commitTransaction();
        if (sales != null && sales.size() > 0) {
            pillArrayList.clear();
            pillArrayList.addAll(sales);
            adapter.notifyDataSetChanged();
            progressDialog.dismiss();
        } else
            progressDialog.dismiss();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
