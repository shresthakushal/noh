package com.example.kusha.noh.appointment;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.kusha.noh.Model.OnClickListener;
import com.example.kusha.noh.R;
import com.example.kusha.noh.report.ReportVH;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Wongel on 7/14/17.
 */

public class AppointmentAdapter extends RecyclerView.Adapter<AppointmentVH> {
    private Context context;
    private List<Appointment> phoneList = new ArrayList<>();

    public AppointmentAdapter(Context context, List<Appointment> phoneList) {
        this.context = context;
        this.phoneList = phoneList;
    }

    @Override
    public AppointmentVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_appointment, parent, false);
        return new AppointmentVH(view);
    }

    @Override
    public void onBindViewHolder(final AppointmentVH holder, final int position) {
        Appointment obj = phoneList.get(position);
        holder.txtAppointmentDate.setText(obj.getAppointmentDate());
        holder.txtAppointmentDay.setText(obj.getAppointmentDay());
        holder.txtAppointmentTime.setText(obj.getAppointmentTime());
        holder.txtDoctor.setText(obj.getDoctor());
        holder.txtSpecialist.setText(obj.getSpecialist());
    }


    @Override
    public int getItemCount() {
        return phoneList.size();
    }
}
