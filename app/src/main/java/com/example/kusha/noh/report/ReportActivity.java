package com.example.kusha.noh.report;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.kusha.noh.Model.OnClickListener;
import com.example.kusha.noh.R;
import com.example.kusha.noh.utils.Constant;
import com.example.kusha.noh.utils.GlobalUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ReportActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager manager;
    private List<String> list = new ArrayList<>();
    private FirebaseFirestore db;
    private RecyclerView.Adapter adapter;
    private ProgressDialog mProgressDialog;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);
        if (Build.VERSION.SDK_INT >= 23) {
            requestPermission();
        } else
            createFolder("NepalOrthopedicHospital");
        initToolbar();
        findViews();
        initRecycleView();
    }

    private void findViews() {
        db = FirebaseFirestore.getInstance();
        recyclerView = findViewById(R.id.recyclerSerialKeyProduct);

    }

    public void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Report");
        getSupportActionBar().setDisplayShowTitleEnabled(true);
    }

    public void initRecycleView() {
        manager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(manager);
        adapter = new ReportAdapter(this, new OnClickListener() {
            @Override
            public void onClicked(int position) {
                new DownloadImage().execute(list.get(position));
            }
        }, list);
        recyclerView.setAdapter(adapter);
        getReports();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void getReports() {
        db.collection("Reports")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            list.clear();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                if (GlobalUtils.getFromPref(Constant.USERNAME, ReportActivity.this).equals(document.getData().get("Username"))) {
                                    list.add(String.valueOf(document.getData().get("Images")));
                                    adapter.notifyDataSetChanged();
                                }
                            }
                        } else {
                            Log.w("Successful", "Error getting documents.", task.getException());
                        }
                    }
                });
    }

    private void requestPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                final AlertDialog alertDialog = new AlertDialog.Builder(this, R.style.DialogTheme)
                        .setTitle("Location  Permission Needed")
                        .setMessage("This app needs the Location permission permission , please accept to use location functionality")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions(ReportActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 5);
                            }
                        })
                        .create();
                alertDialog.setCancelable(false);
                alertDialog.show();
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 5);
            }
        } else {
            createFolder("NepalOrthopedicHospital");
        }
    }


    private class DownloadImage extends AsyncTask<String, Void, Bitmap> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Create a progressdialog
            mProgressDialog = new ProgressDialog(ReportActivity.this);
            // Set progressdialog title
            mProgressDialog.setTitle("Downloading Report");
            // Set progressdialog message
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            // Show progressdialog
            mProgressDialog.show();
        }

        @Override
        protected Bitmap doInBackground(String... URL) {

            String imageURL = URL[0];

            Bitmap bitmap = null;
            try {
                // Download Image from URL
                InputStream input = new java.net.URL(imageURL).openStream();
                // Decode Bitmap
                bitmap = BitmapFactory.decodeStream(input);
                saveImage(bitmap);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            // Set the bitmap into ImageView
            // Close progressdialog
            mProgressDialog.setTitle("Download Successful");
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mProgressDialog.dismiss();
                }
            }, 3000);

        }
    }

    private void saveImage(Bitmap bitmap) {
        String path = Environment.getExternalStorageDirectory().toString();
        OutputStream fOut = null;
        File myDir = new File(path + "/NepalOrthopedicHospital");
        if (!myDir.exists()) {
            myDir.mkdirs();
        }
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        String iname = "NOH-" + n + ".jpg";
        File file = new File(myDir, iname);
        if (file.exists())
            file.delete();
        try {
            fOut = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, fOut); // saving the Bitmap to a file compressed as a JPEG with 85% compression rate
            fOut.flush(); // Not really required
            fOut.close(); // do not forget to close the stream

            MediaScannerConnection.scanFile(this, new String[]{file.getPath()},
                    null, new MediaScannerConnection.OnScanCompletedListener() {
                        public void onScanCompleted(String path, Uri uri) {
                            Log.d("ImagePath", path);
                        }
                    });
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private void createFolder(String fname) {
        String myfolder = Environment.getExternalStorageDirectory().toString() + "/" + fname;
        File f = new File(myfolder);
        try {
            if (!f.exists())
                if (!f.mkdirs()) {
                    Toast.makeText(this, "$myfolder can't be created.", Toast.LENGTH_SHORT).show();
                } else
                    f.mkdirs();
        } catch (Exception ex) {
            Log.d("exception", ex.getLocalizedMessage());
        }
    }

}
