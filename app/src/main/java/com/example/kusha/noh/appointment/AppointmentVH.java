package com.example.kusha.noh.appointment;


import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.kusha.noh.R;


/**
 * Created by sujan on 7/18/17.
 */

public class AppointmentVH extends RecyclerView.ViewHolder {
    TextView txtSpecialist;
    TextView txtDoctor;
    TextView txtAppointmentDate;
    TextView txtAppointmentTime;
    TextView txtAppointmentDay;

    public AppointmentVH(View itemView) {
        super(itemView);
        txtSpecialist = itemView.findViewById(R.id.txtSpecialist);
        txtDoctor = itemView.findViewById(R.id.txtDoctor);
        txtAppointmentDate = itemView.findViewById(R.id.txtAppointmentDate);
        txtAppointmentTime = itemView.findViewById(R.id.txtAppointmentTime);
        txtAppointmentDay = itemView.findViewById(R.id.txtAppointmentDay);
    }
}
