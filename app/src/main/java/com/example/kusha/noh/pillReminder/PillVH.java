package com.example.kusha.noh.pillReminder;


import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.kusha.noh.R;


/**
 * Created by sujan on 7/18/17.
 */

public class PillVH extends RecyclerView.ViewHolder {
    TextView txtName;
    TextView txtTime;

    public PillVH(View itemView) {
        super(itemView);
        txtName = itemView.findViewById(R.id.txtName);
        txtTime = itemView.findViewById(R.id.txtTime);
    }
}
