package com.example.kusha.noh.Model;

public class User {
    private String Address,Age,Email,MembershipCardNo,Name,Password,Username,UId;

    public User() {
    }



    public User(String address, String age, String email, String membershipCardNo, String name, String password, String username) {
        Address = address;
        Age = age;
        Email = email;
        MembershipCardNo = membershipCardNo;
        Name = name;
        Password = password;
        Username=username;
        this.UId = UId;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getAge() {
        return Age;
    }

    public void setAge(String age) {
        Age = age;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getMembershipCardNo() {
        return MembershipCardNo;
    }

    public void setMembershipCardNo(String membershipCardNo) {
        MembershipCardNo = membershipCardNo;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getUId() {
        return UId;
    }

    public void setUId(String UId) {
        this.UId = UId;
    }
}
