package com.example.kusha.noh.pillReminder;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Pill extends RealmObject {
    @PrimaryKey
    private int id;
    private String name;
    private String time;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}

