package com.example.kusha.noh.service;

import android.app.ActivityManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;


import com.example.kusha.noh.HomeActivity;
import com.example.kusha.noh.R;
import com.example.kusha.noh.utils.Config;
import com.example.kusha.noh.utils.Constant;
import com.example.kusha.noh.utils.GlobalUtils;
import com.example.kusha.noh.utils.NotificationUtils;

import java.util.List;

/**
 * Created by Wongel on 7/24/17.
 */

public class AlarmService extends WakeIntentService {
    public AlarmService() {

        super("AlarmService");
    }

    @Override
    void doReminderWork(Intent intent) {
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
        int notifyID = 1;
        String CHANNEL_ID = "my_channel_01";// The id of the channel.
        CharSequence name = "NOH";// The user-visible name of the channel.
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setSound(alarmSound)
                        .setContentTitle(getString(R.string.app_name))
                        .setContentText("Pill Reminder");
        Intent notificationIntent;

        if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
            notificationIntent = new Intent(Config.PUSH_NOTIFICATION);

        } else {
            notificationIntent = new Intent(this, HomeActivity.class);

        }
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent,
                PendingIntent.FLAG_CANCEL_CURRENT);
        builder.setContentIntent(contentIntent);
        builder.setAutoCancel(true);

        // Add as notification
        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel("100", name, importance);
            builder.setChannelId("100");
            mChannel.setDescription("Pill Reminder");
            mChannel.enableLights(true);
            mChannel.setLightColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
            manager.createNotificationChannel(mChannel);
        }
        manager.notify(100, builder.build());
    }


}
