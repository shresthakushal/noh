package com.example.kusha.noh;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.kusha.noh.appointment.AppointmentActivity;
import com.example.kusha.noh.pillReminder.PillReminderActivity;
import com.example.kusha.noh.report.ReportActivity;
import com.example.kusha.noh.utils.Constant;
import com.example.kusha.noh.utils.GlobalUtils;

public class HomeActivity extends AppCompatActivity {
    ImageView appointmentImg, bmiImg, reportImg, pillimg;
    Button btnLogin;

    LinearLayout layout_user_information;
    TextView text_user_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        layout_user_information = findViewById(R.id.layout_user_information);
        text_user_name = findViewById(R.id.txt_user_name);


        appointmentImg = (ImageView) findViewById(R.id.appointment_img);
        appointmentImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (GlobalUtils.getFromPrefBoolean(Constant.IS_USER_LOGIN, HomeActivity.this)) {
                    GlobalUtils.navigateActivity(HomeActivity.this, false, AppointmentActivity.class);
                } else {
                    GlobalUtils.navigateActivity(HomeActivity.this, false, LoginActivity.class);
                }
            }
        });
        btnLogin = findViewById(R.id.btnLogin);
        bmiImg = findViewById(R.id.bmi_img);
        reportImg = findViewById(R.id.report_img);
        pillimg = findViewById(R.id.pillreminder_img);
        layout_user_information.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GlobalUtils.savePrefBoolean(Constant.IS_USER_LOGIN, false, HomeActivity.this);
                GlobalUtils.savePref(Constant.USERNAME, "", HomeActivity.this);
                layout_user_information.setVisibility(View.GONE);
                btnLogin.setVisibility(View.VISIBLE);
            }
        });

        bmiImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, Bmi.class);
                startActivity(intent);
            }
        });
        reportImg = (ImageView) findViewById(R.id.report_img);
        reportImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (GlobalUtils.getFromPrefBoolean(Constant.IS_USER_LOGIN, HomeActivity.this)) {
                    GlobalUtils.navigateActivity(HomeActivity.this, false, ReportActivity.class);
                } else {
                    GlobalUtils.navigateActivity(HomeActivity.this, false, LoginActivity.class);
                }
            }
        });
        pillimg = (ImageView) findViewById(R.id.pillreminder_img);
        pillimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (GlobalUtils.getFromPrefBoolean(Constant.IS_USER_LOGIN, HomeActivity.this)) {
                    GlobalUtils.navigateActivity(HomeActivity.this, false, PillReminderActivity.class);
                } else {
                    GlobalUtils.navigateActivity(HomeActivity.this, false, LoginActivity.class);
                }
            }
        });
        if (GlobalUtils.getFromPrefBoolean(Constant.IS_USER_LOGIN, this)) {
            btnLogin.setVisibility(View.GONE);
            layout_user_information.setVisibility(View.VISIBLE);
            text_user_name.setText(GlobalUtils.getFromPref(Constant.USERNAME, HomeActivity.this));
        } else {
            layout_user_information.setVisibility(View.GONE);


            btnLogin.setVisibility(View.VISIBLE);
            btnLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
                    startActivity(intent);
                }
            });
        }

    }
}
