package com.example.kusha.noh;

import java.util.HashMap;
import java.util.Map;

public class FirebasePost {
    String uname;
    String umembershipcardno;
    String uemail;
    String uaddress;
    String uage;
    String uuser;
    String upass;

    public FirebasePost(String uname, String umembershipcardno, String uemail, String uaddress, String uage, String uuser, String upass) {
        this.uname = uname;
        this.umembershipcardno = umembershipcardno;
        this.uemail = uemail;
        this.uaddress = uaddress;
        this.uage = uage;
        this.uuser = uuser;
        this.upass = upass;
    }

    public Map<String, String> toMap() {
        HashMap<String, String> result = new HashMap();
        result.put("Username", uname);
        result.put("MembershipCard", umembershipcardno);
        result.put("Email", uemail);
        result.put("Address", uaddress);
        result.put("Age", uage);
        result.put("Password", upass);
        return result;
    }
}
