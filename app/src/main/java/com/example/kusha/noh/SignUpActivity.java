package com.example.kusha.noh;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class SignUpActivity extends AppCompatActivity {
    Button SignUp;
    EditText name, address, age, email, membershipcardno, password, repassword, username;
    FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        name = findViewById(R.id.txtName);
        membershipcardno = findViewById(R.id.MembershipCardNo);
        email = findViewById(R.id.Email);
        address = findViewById(R.id.Address);
        age = findViewById(R.id.Age);
        username = findViewById(R.id.Username);
        password = findViewById(R.id.Password);
        repassword = findViewById(R.id.RePassword);
        SignUp = findViewById(R.id.Submit_btn);

        db = FirebaseFirestore.getInstance();

        SignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Uname = name.getText().toString().trim();
                String Umembershipcardno = membershipcardno.getText().toString().trim();
                String Uemail = email.getText().toString().trim();
                String Uaddress = address.getText().toString().trim();
                String Uage = age.getText().toString().trim();
                String Uuser = username.getText().toString().trim();
                String Upass = password.getText().toString().trim();
                String Urepass = repassword.getText().toString().trim();
                if (Uname == "" || Uemail == "" || Uaddress == "" || Uage == "" || Uuser == "" || Upass == "" || Urepass == "") {
                    Toast.makeText(SignUpActivity.this, "Please fill up ", Toast.LENGTH_SHORT).show();
                } else {
                    if (Upass.equals(Urepass)) {
                        SubmitData(Uname, Umembershipcardno, Uemail, Uaddress, Uage, Uuser, Upass);
                    } else {
                        Toast.makeText(SignUpActivity.this, "Password and Re-edtPassword donot match", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });


    }


    private void SubmitData(String uname, String umembershipcardno, String uemail, String uaddress, String uage, String uuser, String upass) {
        Map<String, Object> userMap = new HashMap<>();
        userMap.put("Membership Card No", umembershipcardno);
        userMap.put("Email", uemail);
        userMap.put("Address", uaddress);
        userMap.put("Age", uage);
        userMap.put("Name", uname);
        userMap.put("Password", upass);
        userMap.put("Username", uuser);
        try {
            db.collection("users").add(userMap)
                    .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                        @Override
                        public void onSuccess(DocumentReference documentReference) {
                            Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.w("SuccessFul", "Error adding document", e);
                        }
                    });
        } catch (Exception ex) {
            Log.d("SuccessFul", ex.getLocalizedMessage());

        }
    }
}
