package com.example.kusha.noh.appointment;

import android.app.ProgressDialog;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;

import com.example.kusha.noh.Model.OnClickListener;
import com.example.kusha.noh.R;
import com.example.kusha.noh.report.ReportActivity;
import com.example.kusha.noh.report.ReportAdapter;
import com.example.kusha.noh.utils.Constant;
import com.example.kusha.noh.utils.GlobalUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

public class AppointmentActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager manager;
    private FirebaseFirestore db;
    private FloatingActionButton fabButton;
    private AppointmentAdapter adapter;
    private ProgressDialog progressDialog;
    private ArrayList<Appointment> appointmentArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointment);
        initToolbar();
        findViews();
        evenetHandling();
        initRecycleView();
    }

    private void findViews() {
        db = FirebaseFirestore.getInstance();
        recyclerView = findViewById(R.id.recyclerView);
        fabButton = findViewById(R.id.fabButton);

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (GlobalUtils.getFromPrefBoolean(Constant.LOAD_DATA, this)) {
            GlobalUtils.savePrefBoolean(Constant.LOAD_DATA, false, this);
            getAppointmentList();
        }
    }

    private void evenetHandling() {
        fabButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GlobalUtils.navigateActivity(AppointmentActivity.this, false, AddAppointmentActivity.class);
            }
        });
    }

    public void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Appointment");
        getSupportActionBar().setDisplayShowTitleEnabled(true);
    }

    private void getAppointmentList() {
        showLoading();
        db.collection("Appointment")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            appointmentArrayList.clear();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Appointment appointment = new Appointment();
                                if (GlobalUtils.getFromPref(Constant.USERNAME, AppointmentActivity.this).equals(document.getData().get("Username"))) {
                                    appointment.setAppointmentDate(String.valueOf(document.getData().get("Appointment Date")));
                                    appointment.setAppointmentDay(String.valueOf(document.getData().get("Appointment Day")));
                                    appointment.setAppointmentTime(String.valueOf(document.getData().get("Appointment Time")));
                                    appointment.setDoctor(String.valueOf(document.getData().get("Doctor")));
                                    appointment.setSpecialist(String.valueOf(document.getData().get("Specialist")));
                                }
                                appointmentArrayList.add(appointment);
                            }
                            progressDialog.dismiss();
                            adapter.notifyDataSetChanged();
                        } else {
                            Log.w("Successful", "Error getting documents.", task.getException());
                        }
                    }
                });
    }

    private void showLoading() {
        progressDialog = new ProgressDialog(AppointmentActivity.this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
    }

    public void initRecycleView() {
        manager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(manager);
        adapter = new AppointmentAdapter(this, appointmentArrayList);
        recyclerView.setAdapter(adapter);
        getAppointmentList();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
