package com.example.kusha.noh.appointment;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.kusha.noh.HomeActivity;
import com.example.kusha.noh.LoginActivity;
import com.example.kusha.noh.R;
import com.example.kusha.noh.SignUpActivity;
import com.example.kusha.noh.utils.Constant;
import com.example.kusha.noh.utils.GlobalUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AddAppointmentActivity extends AppCompatActivity {
    private Spinner spnSpecialistType;
    private Spinner spnDoctorType;
    private Spinner spnDayType;
    private RelativeLayout lytDate;
    private RelativeLayout lytTime;
    private FirebaseFirestore db;
    private ArrayAdapter<String> specialistAdapter;
    private ArrayList<String> specialistList = new ArrayList<>();
    private ArrayAdapter<String> doctorAdapter;
    private ArrayList<Doctor> doctorArrayList = new ArrayList<>();
    private ArrayList<String> doctorList = new ArrayList<>();
    private ArrayAdapter<String> dayAdapter;
    private TextView txtScheduleDate;
    private TextView txtAppointmentTime;
    private Button btnSubmit;
    private String specialist;
    private String doctor;
    private String date;
    private String appointmentDay;
    private ArrayList<String> dayList = new ArrayList<>();
    private ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_appointment);
        initToolbar();
        findViews();
        eventHandling();
    }

    private void findViews() {
        db = FirebaseFirestore.getInstance();
        spnSpecialistType = findViewById(R.id.spnSpecialistType);
        spnDoctorType = findViewById(R.id.spnDoctorType);
        spnDayType = findViewById(R.id.spnDayType);
        lytDate = findViewById(R.id.lytDate);
        btnSubmit = findViewById(R.id.btnSubmit);
        txtScheduleDate = findViewById(R.id.txtScheduleDate);
        txtAppointmentTime = findViewById(R.id.txtAppointmentTime);
        lytTime = findViewById(R.id.lytTime);
        setSpinner();
    }

    private void eventHandling() {
        lytDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDateDialog();
            }
        });
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bookAppointment();
            }
        });
        lytTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimeDialog();
            }
        });
        spnSpecialistType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    specialist = specialistList.get(position - 1);
                    getDoctorType();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spnDoctorType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    doctor = doctorList.get(position - 1);
                    getAppointmentDay();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spnDayType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    appointmentDay = dayList.get(position - 1);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void bookAppointment() {
        if (specialist != null && !TextUtils.isEmpty(specialist) &&
                doctor != null && !TextUtils.isEmpty(doctor) &&
                appointmentDay != null && !TextUtils.isEmpty(appointmentDay) &&
                !TextUtils.isEmpty(txtScheduleDate.getText().toString().trim()) &&
                !TextUtils.isEmpty(txtAppointmentTime.getText().toString().trim())) {
            showLoading();
            Map<String, Object> userMap = new HashMap<>();
            userMap.put("Specialist", specialist);
            userMap.put("Doctor", doctor);
            userMap.put("Appointment Day", appointmentDay);
            userMap.put("Appointment Date", txtScheduleDate.getText().toString().trim());
            userMap.put("Appointment Time", txtAppointmentTime.getText().toString().trim());
            userMap.put("Username", GlobalUtils.getFromPref(Constant.USERNAME, this));
            try {
                db.collection("Appointment").add(userMap)
                        .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                            @Override
                            public void onSuccess(DocumentReference documentReference) {
                                progressDialog.dismiss();
                                GlobalUtils.savePrefBoolean(Constant.LOAD_DATA, true, AddAppointmentActivity.this);
                                Toast.makeText(AddAppointmentActivity.this, "Appointment booked for " + specialist + " " + doctor + " on " + txtScheduleDate.getText().toString().trim() + " " + txtAppointmentTime.getText().toString().trim(), Toast.LENGTH_SHORT).show();
                                finish();
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.w("SuccessFul", "Error adding document", e);
                            }
                        });
            } catch (Exception ex) {
                Log.d("SuccessFul", ex.getLocalizedMessage());

            }
        } else {
            Toast.makeText(this, "Field cannot be empty", Toast.LENGTH_SHORT).show();
        }
    }

    private void showLoading() {
        progressDialog = new ProgressDialog(this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
    }


    private void showDateDialog() {
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                        int monthUpdate = month + 1;
                        date = year + "/" + monthUpdate + "/" + day;
                        txtScheduleDate.setText(date);
                    }
                }, year, month, day);
        datePickerDialog.show();
    }

    private void showTimeDialog() {
        Calendar currentTime = Calendar.getInstance();
        int hour = currentTime.get(Calendar.HOUR_OF_DAY);
        int minute = currentTime.get(Calendar.MINUTE);
        TimePickerDialog dialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                updateTime(hourOfDay, minute);
            }
        }, hour, minute, true);
        dialog.show();
    }

    private void updateTime(int hours, int mins) {
        int hoursUpdate = 0;
        String timeSet = "";
        if (hours < 12) {
            hoursUpdate = hours;
            timeSet = "AM";
        } else if (hours > 12) {
            hoursUpdate = hours - 12;
            timeSet = "PM";
        } else if (hours == 0) {
            hoursUpdate = hours + 12;
            timeSet = "AM";
        } else if (hours == 12) {
            timeSet = "PM";
        } else {
            timeSet = "AM";
        }
        String time = String.format("%02d:%02d", hoursUpdate, mins);
        time = time + " " + timeSet;
        txtAppointmentTime.setText(time);
    }

    public void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Add Appointment");
        getSupportActionBar().setDisplayShowTitleEnabled(true);
    }

    private void setSpinner() {
        specialistAdapter = new ArrayAdapter<String>(this, R.layout.spn_filter_layout, R.id.filter, getSpinnerList("Select Specialist", specialistList));
        doctorAdapter = new ArrayAdapter<String>(this, R.layout.spn_filter_layout, R.id.filter, getSpinnerList("Select Doctor", doctorList));
        dayAdapter = new ArrayAdapter<String>(this, R.layout.spn_filter_layout, R.id.filter, getSpinnerList("Select Appointment Day", dayList));
        spnDoctorType.setAdapter(doctorAdapter);
        spnSpecialistType.setAdapter(specialistAdapter);
        spnDayType.setAdapter(dayAdapter);
        getSpecialistData();
    }

    private String[] getSpinnerList(String title, List<String> specialistList) {
        List<String> list = new ArrayList<>();
        if (title != null)
            list.add(title);
        for (String specialist : specialistList)
            list.add(specialist);
        String[] l = new String[list.size()];
        l = list.toArray(l);
        return l;
    }

    private void getSpecialistData() {
        db.collection("Specialist")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                specialistList.add(String.valueOf(document.getData().get("type")));
                            }
                            specialistAdapter = new ArrayAdapter<String>(AddAppointmentActivity.this, R.layout.spn_filter_layout, R.id.filter, getSpinnerList("Select Specialist", specialistList));
                            spnSpecialistType.setAdapter(specialistAdapter);
                        } else {
                            Log.w("Successful", "Error getting documents.", task.getException());
                        }
                    }
                });


    }

    private void getAppointmentDay() {
        dayList.clear();
        for (Doctor doc : doctorArrayList) {
            if (doctor.equals(doc.getName())) {
                dayList.addAll(doc.getAvailableDay());
            }
        }

        dayAdapter = new ArrayAdapter<String>(AddAppointmentActivity.this, R.layout.spn_filter_layout, R.id.filter, getSpinnerList("Select Appointment Day", dayList));
        spnDayType.setAdapter(dayAdapter);

    }

    private void getDoctorType() {
        db.collection("Doctor")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            doctorList.clear();
                            dayList.clear();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                if (specialist.equals(String.valueOf(document.getData().get("Specialist")))) {
                                    Doctor doctor = new Doctor();
                                    doctor.setName(String.valueOf(document.getData().get("Name")));
                                    doctor.setAvailableTime(String.valueOf(document.getData().get("Available Time")));
                                    doctor.setSpecialist(String.valueOf(document.getData().get("Specialist")));
                                    ArrayList<String> list = (ArrayList<String>) document.getData().get("Available Day");
                                    doctor.setAvailableDay(list);
                                    doctorArrayList.add(doctor);
                                    doctorList.add(String.valueOf(document.getData().get("Name")));
                                }
                            }
                            doctorAdapter = new ArrayAdapter<String>(AddAppointmentActivity.this, R.layout.spn_filter_layout, R.id.filter, getSpinnerList("Select Doctor", doctorList));
                            spnDoctorType.setAdapter(doctorAdapter);
                            dayAdapter = new ArrayAdapter<String>(AddAppointmentActivity.this, R.layout.spn_filter_layout, R.id.filter, getSpinnerList("Select Appointment Day", dayList));
                            spnDayType.setAdapter(dayAdapter);
                        } else {
                            Log.w("Successful", "Error getting documents.", task.getException());
                        }
                    }
                });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
