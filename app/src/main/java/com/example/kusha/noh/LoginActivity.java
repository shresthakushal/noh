package com.example.kusha.noh;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.kusha.noh.utils.Constant;
import com.example.kusha.noh.utils.GlobalUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

public class LoginActivity extends AppCompatActivity {
    EditText edtUsername, edtPassword;
    Button btnSignUp, btnLogin;
    String password, username;
    FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        findViews();
        eventHandling();
    }

    private void findViews() {
        db = FirebaseFirestore.getInstance();
        edtUsername = findViewById(R.id.edtUsername);
        edtPassword = findViewById(R.id.edtPassword);

        btnSignUp = findViewById(R.id.btnSignUp);
        btnLogin = findViewById(R.id.btnLogin);
    }

    private void eventHandling() {
        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
                startActivity(intent);
            }
        });
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkLogin();
            }
        });
    }

    private void checkLogin() {
        password = edtPassword.getText().toString().trim();
        username = edtUsername.getText().toString().trim();
        db.collection("users")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                // document.
                                if (username.equals(document.getData().get("Username")) && password.equals(document.getData().get("Password"))) {
                                    GlobalUtils.savePrefBoolean(Constant.IS_USER_LOGIN, true, LoginActivity.this);
                                    GlobalUtils.savePref(Constant.USERNAME, String.valueOf(document.getData().get("Username")), LoginActivity.this);
                                    GlobalUtils.savePref(Constant.FULLNAME, String.valueOf(document.getData().get("Name")), LoginActivity.this);
                                    Toast.makeText(LoginActivity.this, "LoginActivity Successful", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                                    startActivity(intent);
                                    finish();
                                    break;
                                } else {
                                    Toast.makeText(LoginActivity.this, "Invalid username and password", Toast.LENGTH_SHORT).show();
                                    edtUsername.setText("");
                                    edtPassword.setText("");
                                }
                            }
                        } else {
                            Log.w("Successful", "Error getting documents.", task.getException());
                        }
                    }
                });
    }
}
