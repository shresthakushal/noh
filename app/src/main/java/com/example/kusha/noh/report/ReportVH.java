package com.example.kusha.noh.report;


import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.example.kusha.noh.R;


/**
 * Created by sujan on 7/18/17.
 */

public class ReportVH extends RecyclerView.ViewHolder {
    ImageView imgReport;
    RelativeLayout lytDownload;

    public ReportVH(View itemView) {
        super(itemView);
        imgReport = itemView.findViewById(R.id.imgReport);
        lytDownload = itemView.findViewById(R.id.lytDownload);
    }
}
