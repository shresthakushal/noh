package com.example.kusha.noh.appointment;

import java.util.ArrayList;

public class Doctor {
    private String availableTime;
    private String specialist;
    private String name;
    private ArrayList<String> availableDay = new ArrayList<>();

    public String getAvailableTime() {
        return availableTime;
    }

    public void setAvailableTime(String availableTime) {
        this.availableTime = availableTime;
    }

    public String getSpecialist() {
        return specialist;
    }

    public void setSpecialist(String specialist) {
        this.specialist = specialist;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<String> getAvailableDay() {
        return availableDay;
    }

    public void setAvailableDay(ArrayList<String> availableDay) {
        this.availableDay = availableDay;
    }
}
