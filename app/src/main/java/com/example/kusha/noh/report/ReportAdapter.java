package com.example.kusha.noh.report;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.kusha.noh.Model.OnClickListener;
import com.example.kusha.noh.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Wongel on 7/14/17.
 */

public class ReportAdapter extends RecyclerView.Adapter<ReportVH> {
    private Context context;
    private List<String> phoneList = new ArrayList<>();
    private OnClickListener listener;

    public ReportAdapter(Context context, OnClickListener listener, List<String> phoneList) {
        this.context = context;
        this.phoneList = phoneList;
        this.listener = listener;
    }

    @Override
    public ReportVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_report, parent, false);
        return new ReportVH(view);
    }

    @Override
    public void onBindViewHolder(final ReportVH holder, final int position) {
        String obj = phoneList.get(position);
        Picasso.with(holder.itemView.getContext()).load(obj).into(holder.imgReport);
        holder.lytDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onClicked(position);
            }
        });
    }


    @Override
    public int getItemCount() {
        return phoneList.size();
    }
}
