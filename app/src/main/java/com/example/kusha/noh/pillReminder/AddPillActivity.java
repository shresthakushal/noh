package com.example.kusha.noh.pillReminder;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.kusha.noh.R;
import com.example.kusha.noh.appointment.AddAppointmentActivity;
import com.example.kusha.noh.appointment.Doctor;
import com.example.kusha.noh.utils.Constant;
import com.example.kusha.noh.utils.GlobalUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.realm.Realm;

public class AddPillActivity extends AppCompatActivity {
    private EditText edtMedicine;
    private TextView txtAppointmentTime;
    private Button btnSubmit;
    private RelativeLayout lytTime;
    private int hourAlarm = 0;
    private int minAlarm = 0;
    private ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_pill);
        initToolbar();
        findViews();
        eventHandling();
    }

    private void findViews() {
        edtMedicine = findViewById(R.id.edtMedicine);
        btnSubmit = findViewById(R.id.btnSubmit);
        txtAppointmentTime = findViewById(R.id.txtAppointmentTime);
        lytTime = findViewById(R.id.lytTime);
    }

    private void eventHandling() {
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(edtMedicine.getText().toString()) && !TextUtils.isEmpty(txtAppointmentTime.getText().toString().trim())) {
                    showLoading();
                    Pill pill = new Pill();
                    pill.setName(edtMedicine.getText().toString().trim());
                    pill.setTime(txtAppointmentTime.getText().toString().trim());
                    saveData(pill);
                } else if (TextUtils.isEmpty(txtAppointmentTime.getText().toString().trim()))
                    Toast.makeText(AddPillActivity.this, "Please select time", Toast.LENGTH_SHORT).show();
                else if (TextUtils.isEmpty(edtMedicine.getText().toString()))
                    Toast.makeText(AddPillActivity.this, "Please enter medicine name", Toast.LENGTH_SHORT).show();
            }
        });
        lytTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimeDialog();
            }
        });
    }

    private void showLoading() {
        progressDialog = new ProgressDialog(this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
    }


    private void showTimeDialog() {
        Calendar currentTime = Calendar.getInstance();
        final int hour = currentTime.get(Calendar.HOUR_OF_DAY);
        int minute = currentTime.get(Calendar.MINUTE);
        TimePickerDialog dialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                hourAlarm = hourOfDay;
                minAlarm = minute;
                updateTime(hourOfDay, minute);
            }
        }, hour, minute, true);
        dialog.show();
    }

    private void updateTime(int hours, int mins) {
        int hoursUpdate = 0;
        String timeSet = "";
        if (hours < 12) {
            hoursUpdate = hours;
            timeSet = "AM";
        } else if (hours > 12) {
            hoursUpdate = hours - 12;
            timeSet = "PM";
        } else if (hours == 0) {
            hoursUpdate = hours + 12;
            timeSet = "AM";
        } else if (hours == 12) {
            timeSet = "PM";
        } else {
            timeSet = "AM";
        }
        String time = String.format("%02d:%02d", hoursUpdate, mins);
        time = time + " " + timeSet;
        txtAppointmentTime.setText(time);
    }

    public void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Medicine Reminder");
        getSupportActionBar().setDisplayShowTitleEnabled(true);
    }

    private void saveData(final Pill pill) {
        GlobalUtils.setAlarm(AddPillActivity.this, hourAlarm, minAlarm);
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() { // must be in transaction for this to work
            @Override
            public void execute(Realm realm) {
                // increment index
                Number currentIdNum = realm.where(Pill.class).max("id");
                int nextId;
                if (currentIdNum == null) {
                    nextId = 1;
                } else {
                    nextId = currentIdNum.intValue() + 1;
                }
                pill.setId(nextId);
                //...
                realm.insertOrUpdate(pill); // using insert API
                GlobalUtils.savePrefBoolean(Constant.Pill_LOAD_DATA, true, AddPillActivity.this);
                progressDialog.dismiss();
                finish();
            }
        });

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
